class Hotel
	attr_accessor :data

	def initialize(data)
		@data = data
	end


	# Gets hotel's total rates for the given customer_type and reservation dates
	def total_rates(customer_type, dates)
		total = 0
		dates.each do |date|
			day_type = (Date.is_weekend? (date)) ? 'weekend' : 'weekday'
			total   += @data[day_type][customer_type]
		end
		total
	end

end
