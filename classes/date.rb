class Date

	# Checks if given date is a weekend day
	# @param Date date
	# @return Boolean
	def self.is_weekend? (date)
		!![0, 6].index(date.wday)
	end

end
