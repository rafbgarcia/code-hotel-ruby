require 'date'

class Parser

	# Parses a date
	# @param String date
	# @return Date
	def self.date(date)
		date = date.gsub(/^\W/, '')
		Date.parse(date)
	end


	# Parses input data
	# @return Hash
	def self.data(data)
		data = data.chomp.split(':')

		dates = data.last.gsub(/\s+/, '').split(',')
		dates.each_with_index do |date, i|
			dates[i] = self.date(date)
		end

		data = {
			'customer_type' => data.first.strip.downcase,
			'dates'   		=> dates
		}
		data
	end

end
