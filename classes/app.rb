class App

	def init
		for data in STDIN
			data = Parser.data(data)

			puts find_cheapest_hotel(data['customer_type'], data['dates'])
		end
	end


	def find_cheapest_hotel(customer_type, dates)
		cheapest = nil

		for hotel in hotels()
			hotel_rates = hotel.total_rates(customer_type, dates)

			if cheapest != nil
				if hotel_rates < cheapest['total_rates']
					cheapest = hotel.data.merge({'total_rates' => hotel_rates})

				# if has equal total rates, prevails the hotel with highest rating
				elsif hotel_rates == cheapest['total_rates']
					if hotel.data['rating'] > cheapest['rating']
						cheapest = hotel.data.merge({'total_rates' => hotel_rates})
					end
				end
			else
				cheapest = hotel.data.merge({'total_rates' => hotel_rates})
			end
		end

		cheapest['name']
	end


	# Define hotels' data
	def hotels
		[
			(Hotel.new ({
				'name'    => 'Lakewood',
				'rating'  => 3,
				'weekday' => {
					'regular' => 110,
					'rewards' => 80
				},
				'weekend' => {
					'regular' => 90,
					'rewards' => 80
				}
			})),
			(Hotel.new ({
				'name'	  => 'Bridgewood',
				'rating'  => 4,
				'weekday' => {
					'regular' => 160,
					'rewards' => 110
				},
				'weekend' => {
					'regular' => 60,
					'rewards' => 50
				}
			})),
			(Hotel.new ({
				'name'   => 'Ridgewood',
				'rating'  => 5,
				'weekday' => {
					'regular' => 220,
					'rewards' => 100
				},
				'weekend' => {
					'regular' => 150,
					'rewards' => 40
				}
			}))
		]
	end

end
