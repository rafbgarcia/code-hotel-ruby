require 'spec_helper'
require 'date'

describe Date do

	describe "#is_weekend?" do
		it "returns true when the given date's weekday is 0 or 6" do
			# Sunday
			(Date.is_weekend? (Date.new(2012, 11, 4))).should be_true
			# Monday
			(Date.is_weekend? (Date.new(2012, 11, 5))).should be_false
			# Tuesday
			(Date.is_weekend? (Date.new(2012, 11, 6))).should be_false
			# Wednesday
			(Date.is_weekend? (Date.new(2012, 11, 7))).should be_false
			# Thursday
			(Date.is_weekend? (Date.new(2012, 11, 8))).should be_false
			# Friday
			(Date.is_weekend? (Date.new(2012, 11, 9))).should be_false
			# Saturday
			(Date.is_weekend? (Date.new(2012, 11, 10))).should be_true
		end
	end

end
