require 'spec_helper'

describe App do

	before :all do
		@app = App.new
	end

	describe "#find_cheapest_hotel" do
		it "outputs the cheapest hotel for the given data" do

			data = Parser.data 'Regular: 16Mar2009(mon), 17Mar2009(tues), 18Mar2009(wed)'
			(@app.find_cheapest_hotel(data['customer_type'], data['dates'])).should eql 'Lakewood'


			data = Parser.data 'Regular: 20Mar2009(fri), 21Mar2009(sat), 22Mar2009(sun)'
			(@app.find_cheapest_hotel(data['customer_type'], data['dates'])).should eql 'Bridgewood'


			data = Parser.data 'Rewards: 26Mar2009(thur), 27Mar2009(fri), 28Mar2009(sat)'
			(@app.find_cheapest_hotel(data['customer_type'], data['dates'])).should eql 'Ridgewood'

		end
	end

end
