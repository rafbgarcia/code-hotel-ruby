require 'spec_helper'

describe Hotel do


	before :all do
		@hotel = Hotel.new ({
					'name'   => 'Ridgewood',
					'rating'  => 5,
					'weekday' => {
						'regular' => 220,
						'rewards' => 100
					},
					'weekend' => {
						'regular' => 150,
						'rewards' => 40
					}
				})
	end


	describe "#new" do
		it "returns a Hotel object" do
			@hotel.should be_an_instance_of Hotel
		end
		it "throws an ArgumentError if no data is given" do
			lambda{ Hotel.new }.should raise_exception ArgumentError
		end
	end



	# Test the input given in the email
	describe "#total_rates" do
		it "gets the total rates from given customer_type and reservation dates" do
			dates = [
				Parser.date('16Mar2009(mon)'),
				Parser.date('17Mar2009(tues)'),
				Parser.date('18Mar2009(wed)')
			]

			@hotel.total_rates('regular', dates).should eql 3*@hotel.data['weekday']['regular']
		end
	end

end
