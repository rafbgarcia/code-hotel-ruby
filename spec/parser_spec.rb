require 'spec_helper'
require 'date'

describe Parser do

	describe "#date" do
		it "returns a Date object" do
			(Parser.date('16Mar2009(mon)')).should be_an_instance_of Date
		end
	end


	describe "#data" do
		it "returns a hash containing the customer_type and the reservation dates" do
			date = Parser.data('Regular: 16Mar2009(mon), 17Mar2009(tues), 18Mar2009(wed)')

			date.should be_an_instance_of Hash
			date.keys.should eql ['customer_type', 'dates']
			date.values.should eql ['regular', [Date.new(2009, 3, 16), Date.new(2009, 3, 17), Date.new(2009, 3, 18)]]
		end
	end

end
