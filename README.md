# How to run

Install ruby on your machine and simply:

`$ ruby main.rb < input_file`


Ruby version

	$ ruby --version

	ruby 1.9.3p0 (2011-10-30 revision 33570) [i686-linux]


# Testing

For testing, please install RSpec (http://rspec.info/)

	$ gem install rspec

	$ cd path/to/project

	$ rspec
	.......

	Finished in 0.00649 seconds
	7 examples, 0 failures



The tests also include the inputs given in the email. See `spec/app_spec.rb`
